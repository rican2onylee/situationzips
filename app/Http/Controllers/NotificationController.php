<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Notification;

class NotificationController extends Controller
{
    public function getRecent()
    {
        $userId = \Auth::user()->id;

        return Notification::where(
            'user_id', '=', $userId
        )
        ->where('created_at', '>', date('Y-m-d H:i:s', strtotime('-7 days')))
        ->orderBy('created_at', 'desc')
        ->take('10')
        ->get();
    }

    public function see( Request $request )
    {
        $ids = $request->input('ids');

        $notifs = Notification::whereIn('id', $ids)
            ->where('user_id', \Auth::user()->id )
            ->get();

        foreach($notifs as $n)
        {
            $n->is_seen = 1;
            $n->save();
        }  

        return response()->success();
    }
}
