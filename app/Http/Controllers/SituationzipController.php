<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use JWTAuth;
use App\Tag;

class SituationzipController extends Controller
{
    public function __construct( \App\Situationzip $post, \App\Upload $upload )
    {
        $this->post = $post;

        $this->upload = $upload;
    }

    /**
     * Not totally secure.
     *
     * @todo add better validation
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */

    public function create( Request $request )
    {
        $this->validate( $request, [
            'title'       => 'required|string',
            'description' => 'required|string'
        ]);

        $post = $this->makePost( $request->all() );

        $post->save();

        $upload = $this->upload->find( $request->input('media')['id'] );

        $upload->situationzip_id = $post->id;

        $upload->save();

        $this->pushNotify(
            $request->input('recipient')['id'],
            sprintf("<strong>%s</strong> sent you a Battle Request <strong>%s</strong>", \Auth::user()->name, $post->title),
            'post',
            [
                'post'      => $post,
                'recipient' => $request->input('recipient'),
                'sender'    => \Auth::user()
            ]
        );

        return response()->success(['post' => $post]);
    }

    public function read( $id )
    {
        return $this->post->where([
            'id' => $id,
            ['date_accepted', '<', date('Y-m-d H:i:s', strtotime('now'))]
        ])
        ->orWhere([
            'id' => $id,
            'receiver_id' => \Auth::user()->id
        ])
        ->orWhere([
            'id' => $id,
            'sender_id' => \Auth::user()->id
        ])
        ->with([
            'images',
            'likes',
            'recipient' => function($q){
                return $q->with('profile_pic');
            },
            'sender' => function($q){
                return $q->with('profile_pic');
            }
        ])
        ->get()
        ->first();
    }

    public function index( Request $request )
    {
        $query = $this->post
            ->with('images')
            ->with(['likes' => function($q) {
                $q->where('liker_id', \Auth::user()->id);
            }])
            ->with(['recipient' => function($q){
                return $q->with('profile_pic');
            }])
            ->with(['sender' => function($q){
                return $q->with('profile_pic');
            }])
            ->orderBy('created_at','desc');

        $type   = $request->input('post_type');

        $userId = $request->input('related_user', false);

        switch( $type ){

            case "accepted":

                $query->whereNotNull('date_accepted');

            break;
            case "pending":

                $query->whereNull('date_accepted')
                    ->whereNull('date_rejected');

            break;
            default:

                $query->whereNotNull('date_accepted');

            break;
        }

        if($userId){
            $query = $this->setQueryRelatedUser($query, $userId);
        }

        // search keyword
        $keyword = $request->input('keyword');
        if(isset($keyword))
        {
            $words = explode(' ', $keyword);
            $hashtags = [];
            foreach($words as $word)
            {
                if(strpos($word, '#') !== false)
                {
                    array_push($hashtags, str_replace('#', '', $word));
                }
            }

            if($hashtags) {
                $query = $this->setQueryHashtags($query, $hashtags);
            } else {
                $query = $this->setQueryKeyword($query, $keyword);
            }
        }

        return $query->simplePaginate($this->pagination);
    }

    public function setQueryRelatedUser( $query, $userId )
    {
        return $query->where(function($q) use($userId){
            return $q->where('sender_id', $userId)
              ->orWhere('receiver_id', $userId);
        });
    }

    public function setQueryKeyword( $query, $keyword )
    {
        return $query->where(function($q) use($keyword){
            return $q->where('title', 'like', "%{$keyword}%")
                    ->orWhere('description', 'like', "%{$keyword}%")
                    ->orWhere('response_title', 'like', "%{$keyword}%")
                    ->orWhere('response_description', 'like', "%{$keyword}%");
        });
    }

    public function setQueryHashtags( $query, $hashtags )
    {
        return $query
                ->leftJoin('situationzip_tags', 'situationzips.id', '=', 'situationzip_tags.post_id')
                ->leftJoin('tags', 'situationzip_tags.tag_id', '=', 'tags.id')
                ->where(function($q) use($hashtags){
                    return $q->whereIn('tags.value', $hashtags);
                })
                ->select('situationzips.*')
                ->distinct('situationzips.id');
    }

    public function accept( Request $request )
    {
        $post = $this->post->where([
            'receiver_id'   => \Auth::user()->id,
            'id'            => $request->input('id')
        ])
        ->get()
        ->first();

        if(!$post || $post->date_accepted) return abort(403, 'Unauthorized action.');

        $post->response_title       = $request->input('response_title');
        $post->response_description = $request->input('response_description');
        $post->tags                 = json_encode($request->input('tags', []));
        $post->date_accepted        = date('Y-m-d H:i:s', strtotime('now'));
        $post->save();

        $upload = $this->upload->find( $request->input('images')[1]['id'] );

        $upload->situationzip_id = $post->id;

        $upload->save();

        return $post;
    }

    public function makePost( array $params )
    {
        $post = new $this->post;

        $post->title         = $params['title'];
        $post->description   = $params['description'];

        $post->sender_id     = \Auth::user()->id;
        $post->receiver_id   = $params['recipient']['id'];

        $tags = [];
        foreach($params['tags'] as $tg)
        {
            $tag = Tag::firstOrCreate(array('value' => $tg));
            array_push($tags, $tag->id);
        }

        $post->save();
        $post->tags()->sync($tags);

        return $post;
    }
}
