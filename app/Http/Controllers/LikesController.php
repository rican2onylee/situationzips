<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\SituationzipLike;
use App\Situationzip;
use Auth;

class LikesController extends Controller
{
    public function __construct( Situationzip $post, SituationzipLike $like )
    {
        $this->post = $post;
        $this->like = $like;
    }

    public function index( Request $request )
    {
        $post_id   = $request->input('post_id');

        $query = $this->like
            ->where('post_id', $post_id)
            ->where('liker_id', \Auth::user()->id)
            ->orderBy('created_at','desc');

        return $query->simplePaginate($this->pagination);
    }

    public function likePost( Request $request )
    {
        // get post instance
        $post = $this->post->where('id', $request->input('post_id'))->first();

        // create like instance
        $like = (new $this->like);
        $like->liker_id = Auth::user()->id;
        $like->post_id = $request->input('post_id');
        $like->liked_user_id = $request->input('liked_user_id');
        $like->save();

        // update likes count
        if ($request->input('liked_user_id') == $post->receiver_id)
        {
            $post->receiver_like_count = $post->receiver_like_count + 1;
        }

        if ($request->input('liked_user_id') == $post->sender_id)
        {
            $post->sender_like_count = $post->sender_like_count + 1;
        }
        $post->save();

        return $like;
    }

    public function unlikePost( $id, $user_id )
    {
        // get like instance
        $like = $this->like
            ->where('post_id', $id)
            ->where('liker_id', Auth::user()->id)
            ->where('liked_user_id', $user_id)
            ->first();

        if(!$like) return response()->json([], 404);

        $like->delete();

        // update likes count
        $post = $this->post->find($id);


        if ($like->liked_user_id == $post->receiver_id)
        {
            $post->receiver_like_count = $post->receiver_like_count - 1;
        }

        if ($like->liked_user_id == $post->sender_id)
        {
            $post->sender_like_count = $post->sender_like_count - 1;
        }
        $post->save();


        return $like;
    }
}
