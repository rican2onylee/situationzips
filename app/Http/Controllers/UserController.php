<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Tag;
use App\UserTag;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function __construct( User $user )
    {
        $this->user = $user;
    }

    public function me()
    {
        $u = User::where('id', Auth::user()->id)
            ->with('banner_pic', 'profile_pic')
            ->get()
            ->first();

        return response()
            ->success(['user'=>$u]);
    }

    public function meUpdate(Request $request)
    {
        $user    = Auth::user();

        $newUser = (object) $request->input('user');

        $user->about           = $newUser->about;
        $user->banner_pic_id   = $newUser->banner_pic['id'];
        $user->profile_pic_id  = $newUser->profile_pic['id'];

        $user->save();

        $tags = [];
        foreach($newUser->interests as $interest)
        {
            $tag = Tag::firstOrCreate(array('value' => $interest));
            array_push($tags, $tag->id);
        }

        $user->tags()->sync($tags);

        return $user;
    }

    public function user($id)
    {
        return User::where('id', $id)
            ->with('banner_pic', 'profile_pic')
            ->get()
            ->first();
    }



    public function search( Request $request )
    {
        $user    = Auth::user();
        $keyword = $request->input('keyword');

        $query   = $this->user
            ->with('banner_pic', 'profile_pic');

        $users = isset($keyword) ?
            $query
                ->where('name', 'like', "%{$keyword}%")
                ->where('id', '!=', $user->id)
                ->get() :
            $query->where('id', '!=', $user->id)->get();

        if($users)
        {
            return response()->success(compact('users'));
        }

        return response()->error([], 404);
    }
}
