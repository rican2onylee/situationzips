<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Upload;

use App\Http\Requests;

class UploadController extends Controller
{
    public function __construct( Upload $upload )
    {
        $this->upload = $upload;
    }

    public function addMedia( Request $request )
    {
        $media = $request->file('media') ?: $request->input('url');

        if(is_string($media))
        {
            return $this->storeUrlMedia( (object) $request->all() );
        }

        return $this->storeFileMedia( $media );
    }

    public function storeFileMedia( $media )
    {
        $fileName = uniqid(
            basename($media->path())
        ).".".$media->extension();

        $media->move(public_path().'/uploads', $fileName );

        $upload = (new $this->upload);

        $upload->file_name = '/uploads/'.$fileName;
        $upload->type      = 'upload';
        $upload->user_id   = \Auth::user()->id;
        $upload->save();

        return $upload;
    }

    public function storeUrlMedia( $media )
    {
        $upload = (new $this->upload);

        $upload->file_name   = $media->file_name;
        $upload->resource_id = $media->video_id;
        $upload->type        = $media->type;
        $upload->user_id     = \Auth::user()->id;
        $upload->save();

        return $upload;
    }
}
