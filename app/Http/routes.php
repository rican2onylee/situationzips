<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'AngularController@serveApp');

    Route::get('/unsupported-browser', 'AngularController@unsupported');
});

//public API routes
$api->group(['middleware' => ['api']], function ($api) {

    // Authentication Routes...
    $api->post('auth/login', 'Auth\AuthController@login');
    $api->post('auth/register', 'Auth\AuthController@register');

    // Password Reset Routes...
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');
});

//protected API routes with JWT (must be logged in)
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {

    $api->group(['prefix' => 'post'], function($api){

        $api->get('/', 'SituationzipController@index');

        $api->post('/create', 'SituationzipController@create');

        $api->get('/{id}', 'SituationzipController@read');

        $api->post('/accept', 'SituationzipController@accept');
    });

    $api->group(['prefix' => 'notifications'], function($api){

        $api->get('/', 'NotificationController@getRecent');

        $api->post('/', 'NotificationController@see');
    });

    $api->group(['prefix' => 'upload'], function($api){

        $api->post('/', 'UploadController@addMedia');
    });


    $api->group(['prefix' => 'users'], function($api){

        $api->get('/search', 'UserController@search');

        $api->get('/me', 'UserController@me');

        $api->post('/me', 'UserController@meUpdate');

        $api->get('/{id}', 'UserController@user');
    });

    $api->group(['prefix' => 'like'], function($api){

        $api->get('/', 'LikesController@index');

        $api->post('/', 'LikesController@likePost');

        $api->delete('/{id}/{user_id}', 'LikesController@unlikePost');
    });
});
