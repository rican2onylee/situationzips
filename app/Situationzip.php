<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situationzip extends Model
{
    /**
     * The attributes appended to the model's JSON form.
     *
     * @var array
     */
    protected $appends = [
        'tags'
    ];

    public function images()
    {
        return $this->hasMany('App\Upload', 'situationzip_id', 'id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function likes()
    {
        return $this->hasMany('App\SituationzipLike', 'post_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'situationzip_tags', 'post_id', 'tag_id')->withTimestamps();
    }

    public function getTagsAttribute()
    {
        return $this->tags()->pluck('value')->toArray();
    }
}
