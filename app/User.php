<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes appended to the model's JSON form.
     *
     * @var array
     */
    protected $appends = [
        'interests'
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'user_tags')->withTimestamps();
    }

    public function getInterestsAttribute()
    {
        return $this->tags()->pluck('value')->toArray();
    }

    public function profile_pic()
    {
        return $this->hasOne('App\Upload', 'id', 'profile_pic_id');
    }

    public function banner_pic()
    {
        return $this->hasOne('App\Upload', 'id', 'banner_pic_id');
    }

}
