<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SituationzipCreateTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->post('/post/create', [
            'title'       => 'new post example',
            'description' => 'describe the new post'
        ])
        ->seeJsonObject('post')
        ->seeJsonKeyValueString('title', 'new post example')
        ->seeJsonKeyValueString('description', 'describe the new post');


        $this->seeInDatabase('situationzips', [
            'title'       => 'new post example',
            'description' => 'describe the new post'
        ]);
    }
}
