<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLikesCountColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('situationzips', function (Blueprint $table) {
            $table->integer('receiver_like_count')->nullable();
            $table->integer('sender_like_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('situationzips', function (Blueprint $table) {
            $table->dropColumn('receiver_like_count');
            $table->dropColumn('sender_like_count');
        });
    }
}
