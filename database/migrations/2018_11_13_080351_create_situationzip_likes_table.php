<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSituationzipLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('situationzip_likes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liker_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('liked_user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('situationzip_likes');
    }
}
