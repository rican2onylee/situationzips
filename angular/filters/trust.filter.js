export function TrustFilter($sce){
    'ngInject';

    return function( input ){
        return $sce.trustAsHtml(input);
    }
}
