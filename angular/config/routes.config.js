export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},//{auth: true} would require JWT auth
			views: {
				header: {
					templateUrl: getView('header')
				},
				footer: {
					templateUrl: getView('footer')
				},
				main: {}
			}
		})
		.state('app.home', {
            url: '/',
            data: {
                auth: true
            },
            views: {
                'main@': {
                    templateUrl: getView('index')
                }
            }
        })
        .state('app.login', {
			url: '/login',
			views: {
				'main@': {
					templateUrl: getView('login')
				}
			}
		})
        .state('app.register', {
            url: '/register',
            views: {
                'main@': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            data: {
                auth: true
            },
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            }
        })
        .state('app.me', {
            url : '/me',
            data: {
                auth: true
            },
            resolve: {
                authUser : (UserService)=>{
                    'ngInject';      

                    return UserService.getUser();
                }
            },
            views: {
                'main@': {
                    templateUrl: getView('user_profile')
                }
            }
        })
        .state('app.profile', {
            url : '/user/:id',
            data: {
                auth: true
            },
            resolve: {
                authUser : (UserService, $stateParams)=>{
                    'ngInject';      

                    return UserService.getUser($stateParams.id);
                }
            },
            views: {
                'main@': {
                    templateUrl: getView('user_profile')
                }
            }
        })        

        .state('app.post', {
            url: '/posts',
            data: {
                auth: true
            },
            views: {
                'main@' : {
                    templateUrl: getView('create_post')
                }
            }
        })
        .state('app.post.accept', {
            url: '/accept/:id',
            data: {
                auth: true
            },
            views: {
                'main@' : {
                    templateUrl: getView('accept_battle')
                }
            }
        });
}
