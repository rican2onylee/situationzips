export function CustomApiPathsConfig(RestangularProvider){
    'ngInject';

    // It will transform all building elements, NOT collections
    RestangularProvider.addElementTransformer('users', true, function(user) {
            // This will add a method called evaluate that will do a get to path evaluate with NO default
            // query params and with some default header
            // signature is (name, operation, path, params, headers, elementToPost)

            user.addRestangularMethod('search', 'get', 'search', undefined, {});

            return user;
    });
}
