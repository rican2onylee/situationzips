import {ImageResolveDirective} from './directives/image_resolve/image_resolve.directive';
import {BgImageDirective} from './directives/bg_image/bg_image.directive';
import {InputFocusDirective} from './directives/input-focus/input-focus.directive';
import {InputSwapDirective} from './directives/input-swap/input-swap.directive';
import {RlModalDirective} from './directives/rl-modal/rl-modal.directive';

angular.module('app.directives')
	.directive('imageResolve', ImageResolveDirective)
	.directive('bgImage', BgImageDirective)
	.directive('inputFocus', InputFocusDirective)
	.directive('inputSwap', InputSwapDirective)
	.directive('rlModal', RlModalDirective);
