export class UserService {
    constructor($cacheFactory, API, $timeout, $window) {
        'ngInject';

        this.API = API;
        this.cache = $cacheFactory('auth');

        this.userPromise = null;

        this.$timeout = $timeout;
        this.$window = $window;
    }

    resetUser() {
         this.cache.remove('user');
         this.user = false;

         return this.getUser();
    }

    getUser(id) {

        if(id) {
            return this.API.all('users')
                .get(id)
                .then((resp)=>{

                    return resp.user;
            });
        }

        let vm = this;

        if(this.cache.get('user')) {
            return this.$timeout(function() {
                return vm.cache.get('user');
            });
        }

        if(!this.user) {
            return this.API
                .all('users/me')
                .get('')
                .then((resp) => {
                    this.cache.put('user', resp.data.user);

                    this.user = resp.data.user;

                    return this.user;
            })
        }


    }

    setAuthUser(data) {
        this.$window.localStorage.setItem("authUser", JSON.stringify(data));
    }

    getAuthUser() {
        return JSON.parse(this.$window.localStorage.getItem("authUser"));
    }
}

