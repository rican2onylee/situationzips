function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

export class RlModalService{
    constructor(){

        this.modals = {};
    }

    addModal( id, modal ){

        this.modals[id] = modal;

        return this;
    }

    get( id ){

        return this.modals[id];
    }
}

