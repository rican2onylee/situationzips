class InstagramController {
    constructor() {
        'ngInject';

        let vm = this;
    }

    $onInit() { }
}

export const InstagramComponent = {
    templateUrl: './views/app/components/instagram/instagram.component.html',
    controller: InstagramController,
    controllerAs: 'vm',
    bindings: {
        media : '<'
    }
}
