class UploadMediaController {
    constructor(Upload, $scope, API, ToastService) {
        'ngInject';

        this.uploader = Upload;
        this.toastService = ToastService;
        this.API = API;

        this.media = {
            type: 'upload',
            file: null,
            url : ''
        };

        this.ratio = '10:1';

        $scope.$watch('vm.media.file', (value) => {
            if (this.media.type === 'upload') {
                $scope.vm.uploadMedia();
            }
        });
    }

    setType(type) {
        this.media.type = type;
    }

    reset() {
        this.media = {type:"upload"};
    }

    save(media) {
        this.media = media;
        this.uploadParams.onSelect(angular.copy(media, {}), this);
    }

    saveInstagram(media) {
        let id = this.getInstagramId(media.url);
        if (id === null) {
            return // exit if couldn't find id
        }

        media.file_name = `https://www.instagram.com/p/${id}/media/?size=m`;
        media.video_id = id;
        media.type = 'instagram';

        this.uploadSocialMedia(media);
    }

    saveYoutube(media) {
        let id = this.getYoutubeVideoId(media.url);

        media.file_name = `http://i3.ytimg.com/vi/${id}/maxresdefault.jpg`;
        media.video_id = id;
        media.type = 'youtube';

        this.uploadSocialMedia(media);
    }

    getYoutubeVideoId(url) {
        let link = document.createElement('a');
        link.href = url;

        //get the value of the "v" query param

        if(link.search.indexOf('?v=') !== -1) {
            return link.search.split('&')[0].replace('?v=', '');
        }

        //or just get the last string of the url

        let r = url.split('/');

        return r[r.length - 1];
    }

    getInstagramId(url) {
        let id = /p\/(.*?)\/$/.exec(url)
        if (id) {
            return id[1];
        }
        this.toastService.error('Oops! The url was wrong. Please try again!');
        return null;
    }

    uploadMedia() {
        if(this.media.file) {
            this.upload(this.media.file);
        }
    }

    uploadSocialMedia(media) {
        let vm = this;

        this.API
            .all('upload')
            .post(media)
            .then((resp) => {
                media = resp.upload;

                media.preview_url = media.file_name;

                vm.save(media);
            });
    }

    upload(file) {
        let _this = this;

        this.uploader.upload({
            url: '/api/upload',
            data: {
                media : file
            }
        }).then((resp) => {
            _this.media = resp.data.upload;

            _this.media.preview_url = resp.data.upload.file_name;
        },(resp) => {
            this.toastService.error(resp.data);
        },(evt) => {
            let progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '%');
        });
    }

    $onInit() { }
}

export const UploadMediaComponent = {
    templateUrl: './views/app/components/upload_media/upload_media.component.html',
    controller: UploadMediaController,
    controllerAs: 'vm',
    bindings: {
        uploadParams : '<'
    }
}
