class LikeController {
    constructor(API) {
        'ngInject';

        this.API = API;

        this.like = false;
        this.liked_user = null;
    }

    likePost() {
        if (!this.liked_user || !this.post) { return; }

        if (!this.like) {
            let data = {
                post_id: this.post.id,
                liked_user_id: this.liked_user.id,
            }

            this.API
                .all('like')
                .post(data)
                .then((resp) => {
                    this.like = true;
                    this.post.likes.push(resp.situationzip_like);

                    if (this.side === 'sender') {
                        this.post.sender_like_count = this.post.sender_like_count || 0;
                        this.post.sender_like_count += 1;
                    } else {
                        this.post.receiver_like_count = this.post.receiver_like_count || 0;
                        this.post.receiver_like_count += 1;
                    }

                    this.onSave();
                });
        } else {
            const index = this.post.likes.findIndex(lk => lk.liked_user_id === this.liked_user.id);
            if (index === -1) { return; }

            let like = this.post.likes[index];
            this._removeLike(like);
        }
    }

    $onInit() {
        this.like = false;

        if (this.side === 'sender') {
            this.liked_user = this.post.sender;
            this.like = this.post.likes.some(like => like.liked_user_id === this.liked_user.id);
        } else {
            this.liked_user = this.post.recipient;
            this.like = this.post.likes.some(like => like.liked_user_id === this.liked_user.id);
        }
    }

    _removeLike(like) {
        this.API
            .all(`like/${like.post_id}/${like.liked_user_id}`)
            .remove()
            .then((resp) => {
                this.like = false;

                if (this.side === 'sender') {
                    this.post.sender_like_count = this.post.sender_like_count || 1;
                    this.post.sender_like_count -= 1;
                } else {
                    this.post.receiver_like_count = this.post.receiver_like_count || 1;
                    this.post.receiver_like_count -= 1;
                }

                this.onSave();
            });
    }
}

export const LikeComponent = {
    templateUrl: './views/app/components/like/like.component.html',
    controller: LikeController,
    controllerAs: 'vm',
    bindings: {
        post : '=',
        onSave : '&',
        side : '@'
    }
}
