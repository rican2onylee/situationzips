class MessageSendSuccessController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const MessageSendSuccessComponent = {
    templateUrl: './views/app/components/message_send_success/message_send_success.component.html',
    controller: MessageSendSuccessController,
    controllerAs: 'vm',
    bindings: {
        recipient  : '<',
        attachment : '<',
        redirect   : '<',
        message    : '<'
    }
}
