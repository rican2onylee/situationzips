class FindUserFormContextController{
    constructor(API, $rootScope, RlModalService){
        'ngInject';
        
        //initialize with search context
        this.API = API;
        this.keyword = ''; 
    }

    search( keyword ){      

        //perform search with provided keyword
        return this.API 
            .all('users')
            .get('search', {keyword: keyword})
            .then((rsp)=>{
                //store results inside user result array

                return rsp.data.users;
            });
    }

    selectUser( user ){

        if(!this.findUserParams.onSelect){
            throw new Error('No callback defined for user selection');
        }

        this.findUserParams.onSelect( user, this );
    }

    reset(){

        this.selection = null;
        this.keyword = '';
    }

    $onInit(){
    }
}

export const FindUserFormContextComponent = {
    templateUrl: './views/app/components/find_user_form_context/find_user_form_context.component.html',
    controller: FindUserFormContextController,
    controllerAs: 'vm',
    bindings: {
        findUserParams : '<'
    }
}
