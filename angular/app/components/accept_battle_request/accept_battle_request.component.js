import * as $ from 'jquery';

class AcceptBattleRequestController {
    constructor(API, ToastService, RlModalService, $scope, $stateParams, $anchorScroll){
        'ngInject';

        let vm = this;

        vm.API      = API;
        vm.Scroll   = $anchorScroll;
        vm.accepted = false;
        vm.media = {}

        vm.API
            .one('post', $stateParams.id)
            .get()
            .then((resp) => {
                vm.post = resp.situationzip;

                vm.post.response_tags = [];

                if (this.post.images.length > 0) {
                    vm.media = vm.post.images[0];

                    if (vm.media.type === 'instagram') {
                        $.ajaxSetup({
                            scriptCharset: "utf-8",
                            contentType: "application/json; charset=utf-8"
                        });

                        $.getJSON(`https://www.instagram.com/p/${vm.media.resource_id}/?__a=1`,
                            function(data) {
                                if (data && data.graphql) {
                                    vm.media.video_id = data.graphql.shortcode_media.video_url
                                }
                        });
                    }
                }
            });

        this.Toast = ToastService;
        this.modal = RlModalService;
        this.tags  = [];
    }

    accept() {
        this.accepted = true;
    }

    decline() {
        //send decline request and redirect home with flash message
    }

    showPostMedia() {
        let modal = this.modal.get('post_media');

        modal.open({
            title: this.post.title,
            media: this.media
        });
    }

    showUploadModal() {
        let modal = this.modal.get('upload_options');

        let ctrl = this;

        modal.open({
            title: 'Add Media',
            uploadParams : {
                onSelect: function( media ) {
                    modal.close();

                    ctrl.post.images[1] = media;
                }
            }
        });
    }

    submit() {
        if(!this.post.response_title       ||
           !this.post.response_description ||
           !this.post.images[1]       ){

            this.Toast.error('Missing Fields');

            return false;
        }

        let vm = this;

        if(vm.tags.length) {
            vm.post.tags = vm.post.tags.concat(vm.tags);
        }

        this.API
            .all('post/accept')
            .post(vm.post)
            .then((resp) => {
                vm.post = {
                    redirect: 'app.home',
                    attachment: this.post,
                    message: {
                        heading: 'You accepted '+vm.post.recipient.name+"'s Battle Request",
                        body : 'You\'ve Accepted ' + vm.post.recipient.name + '\'s Battle Request!'
                    },
                    recipient: vm.post.recipient
                }

                vm.postSuccess = true;
            });
    }

    $onInit() { }
}

export const AcceptBattleRequestComponent = {
    templateUrl: './views/app/components/accept_battle_request/accept_battle_request.component.html',
    controller: AcceptBattleRequestController,
    controllerAs: 'vm',
    bindings: {}
}
