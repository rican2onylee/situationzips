class PeopleSearchController{
    constructor(API){
        'ngInject';

        this.API = API;

        this.keyword = '';
    }
    search( keyword ){      

        let vm = this;

        //perform search with provided keyword
        return this.API 
            .all('users')
            .get('search', {keyword: keyword})
            .then((rsp)=>{

                vm.users = rsp.data.users;
            });
    }

    $onInit(){
    }
}

export const PeopleSearchComponent = {
    templateUrl: './views/app/components/people_search/people_search.component.html',
    controller: PeopleSearchController,
    controllerAs: 'vm',
    bindings: {}
}
