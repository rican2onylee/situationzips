class PostListController {
    constructor(API) {
        'ngInject';

        this.API = API;
    }

    likePost(post) {
        post.liked = true;

        const receiver_like_count = post.receiver_like_count || 0;
        const sender_like_count = post.sender_like_count || 0;

        const total_likes = receiver_like_count + sender_like_count;

        post.receiver_like_stat = total_likes === 0 ? 0 : (receiver_like_count/total_likes)*100;
        post.sender_like_stat = total_likes === 0 ? 0 : (sender_like_count/total_likes)*100;
    }

    $onInit() {
        this.API
            .all('post')
            .get('', {
                post_type: this.postType,
                related_user : this.relatedUser
            })
            .then((resp) => {
                this.posts = resp.data;
        });
    }
}

export const PostListComponent = {
    templateUrl: './views/app/components/post_list/post_list.component.html',
    controller: PostListController,
    controllerAs: 'vm',
    bindings: {
        relatedUser : '=',
        postType    : '='
    }
}
