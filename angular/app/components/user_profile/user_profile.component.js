class UserProfileController{
    constructor(API, UserService, RlModalService){
        'ngInject';

        this.editable = [];

        this.API      = API;

        this.modal    = RlModalService;

        this.interestTag = '';

        this.UserService = UserService;

        this.contentName = 'browse';

        this.showDetails = false;
    }

    toggleDetails(){
        this.showDetails = !this.showDetails;
    }

    getBannerPic(user){
        if (user && user.banner_pic) {
            if(user.banner_pic.preview_url)
                return user.banner_pic.preview_url;

            if(user.banner_pic.file_name)
                return user.banner_pic.file_name;
        }

        return 'http://placehold.it/1300x250';
    }

    getProfilePic(user){
        if (user && user.profile_pic) {
            if(user.profile_pic.preview_url)
                return user.profile_pic.preview_url;

            if(user.profile_pic.file_name)
                return user.profile_pic.file_name;
        }

        return '/img/profile.png';
    }

    setContent(name, e){
        this.contentName = name;
        e.preventDefault();
    }


    openThumbnailModal(){

        let modal = this.modal.get('thumbnail_upload');

        let ctrl = this;

        modal.open({
            title: 'Set Profile Pic',
            uploadParams : {
                onSelect: function( media ){

                    modal.close();

                    ctrl.edit.user.profile_pic = media;

                    ctrl.edit.user.profilePic  = ctrl.edit.user.profile_pic.preview_url;

                    ctrl.isChanged = true;
                }
            }
        });
    }

    getUserCopy(){
        this.user.bannerPic  = this.getBannerPic(this.user);

        this.user.profilePic = this.getProfilePic(this.user);

        return angular.copy(this.user);
    }

    openBannerModal(){

        let modal = this.modal.get('banner_upload');

        let ctrl = this;

        modal.open({
            title: 'Set Banner Pic',
            uploadParams : {
                onSelect: function( media ){

                    modal.close();

                    ctrl.edit.user.banner_pic = media;

                    ctrl.edit.user.bannerPic  = ctrl.edit.user.banner_pic.preview_url;

                    ctrl.isChanged = true;
                }
            }
        });
    }

    toggleEditable(inputName){
        if (this.loggedUser && this.user && this.loggedUser.id !== this.user.id) { return; } // prevent changing other users

        this.editable[inputName] = !this.editable[inputName];

        if(this.editable[inputName] === true) this.isChanged = true;
    }

    appendInterest(interest){

        if(!this.edit.user.interests) this.edit.user.interests = [];

        if(this.edit.user.interests.indexOf(interest) === -1) {
            this.edit.user.interests.push(interest);
        }

        this.interestTag = '';
    }

    cancelEdit(){
        this.edit.user = this.getUserCopy();

        this.editable  = {};

        this.isChanged = false;
    }

    save(){
        let user = this.edit.user;

        this.API
            .all('users/me')
            .post({
                user: user
            })
            .then((resp)=>{
                this.UserService
                    .resetUser()
                    .then((user)=>{
                        this.user = user;

                        this.isChanged = false;

                        this.editable  = {};
                    });
        });
    }

    $onInit(){
        this.user = this.authUser;

        if (this.user) {
            this.user.bannerPic  = this.getBannerPic(this.user);

            this.user.profilePic = this.getProfilePic(this.user);
        }

        this.edit = {
            user : angular.copy(this.user)
        };

        this.loggedUser = this.UserService.getAuthUser();
    }
}

export const UserProfileComponent = {
    templateUrl: './views/app/components/user_profile/user_profile.component.html',
    controller: UserProfileController,
    controllerAs: 'vm',
    bindings: {
        authUser : '<'
    }
}
