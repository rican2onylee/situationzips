class CreatePostFormController{
    constructor(API, ToastService, RlModalService, $scope){
        'ngInject';

        this.API   = API;
        this.Toast = ToastService;
        this.modal = RlModalService;

        this.title = '';
        this.description = '';
        this.tags = [];
    }

    showUploadModal(){

        let modal = this.modal.get('upload_options');

        let ctrl = this;

        modal.open({
            title: 'Add Media',
            uploadParams : {
                onSelect: function( media ){

                    modal.close();

                    ctrl.media = media;
                }
            }
        });
    }

    chooseRecipient(){

        var ctrl = this;

        var modal = this.modal.get('find_user');

        modal.open({
            title: 'Choose a Battler',
            userParams : {
                onSelect: function( battler, vm ){

                    modal.close();

                    vm.reset();

                    ctrl.recipient = battler;
                }
            }
        });
    }    

    submit(){
        if(!this.title || !this.description || !this.media || !this.recipient){

            this.Toast.error('Missing Fields');

            return false;
        }

        let vm = this;

        let post = {
            title : this.title, 
            description: this.description,
            tags : this.tags,
            media : this.media,
            recipient : this.recipient
        };

        this.API
            .all('post/create')
            .post(post)
            .then(() => {
                //this.Toast.show('Post "' + this.title + '" Created');

                vm.post = {
                    redirect: 'app.home',
                    attachment: post,
                    message: {
                        name : 'Battle Request',
                        body : 'You\'ll be notified if ' + vm.recipient.name + ' accepts your request!'
                    },
                    recipient: vm.recipient
                }

                vm.postSuccess = true;
            });
    }

    $onInit(){
    }
}

export const CreatePostFormComponent = {
    templateUrl: './views/app/components/create_post_form/create_post_form.component.html',
    controller: CreatePostFormController,
    controllerAs: 'vm',
    bindings: {}
}
