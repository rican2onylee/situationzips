class SearchInputController {
    constructor(API) {
        'ngInject';

        this.API = API;
        this.keyword = null;
    }

    $onInit() {}

    search(keyword) {
        this.API
            .all('post')
            .get('', {
                post_type: this.postType,
                related_user : this.relatedUser,
                keyword: this.keyword
            })
            .then((resp) => {
                this.posts = resp.data;
        });
    }
}

export const SearchInputComponent = {
    templateUrl: './views/app/components/search_input/search_input.component.html',
    controller: SearchInputController,
    controllerAs: 'vm',
    bindings: {
        posts : '=',
        postType: '<',
        relatedUser: '<'
    }
}
