class NotificationController{
    constructor(API){
        'ngInject';

        this.API = API;

        this.notifications = [];

        this.getNotifications()

        setInterval(()=>{

            this.getNotifications();
        }, 10000);
    }

    getNotifications(force){

        let vm = this;

        this.API.all('notifications')
            .get('')
            .then((resp)=>{

                if( (!vm.notifications.length) ||
                    force ||
                    vm.notifications && vm.notifications[0].id != resp.notifications[0].id){

                    vm.notifications = resp.notifications;
                }
        });
    }

    getUnseenCount(){

        let count = 0;

        this.notifications = (this.notifications || []);

        for(var i = 0; this.notifications[i] != undefined; i++){

            if(this.notifications[i].is_seen == 0) count++;
        }

        return count;
    }

    see(){

        if(this.getUnseenCount()) return;

        let ids = this.notifications.map((n)=>{
            return n.id;
        });

        let vm = this;

        this.API.all('notifications')
            .post({ids: ids})
            .then((resp)=>{

                vm.getNotifications(true);
            });
    }

    $onInit(){
    }
}

export const NotificationComponent = {
    templateUrl: './views/app/components/notification/notification.component.html',
    controller: NotificationController,
    controllerAs: 'vm',
    bindings: {}
}
