import {ImageResolverService} from './services/image_resolver.service';
import {UserService} from './services/User.service';
import {RlModalService} from './services/rl-modal.service';
import {APIService} from './services/API.service';
import {DialogService} from './services/dialog.service';
import {ToastService} from './services/toast.service';

angular.module('app.services')
	.service('ImageResolverService', ImageResolverService)
	.service('UserService', UserService)
	.service('RlModalService', RlModalService)
	.service('API', APIService)
	.service('DialogService', DialogService)
	.service('ToastService', ToastService)
