import {PeopleSearchComponent} from './app/components/people_search/people_search.component';
import {UserProfileComponent} from './app/components/user_profile/user_profile.component';
import {AcceptBattleRequestComponent} from './app/components/accept_battle_request/accept_battle_request.component';
import {NotificationComponent} from './app/components/notification/notification.component';
import {PostListComponent} from './app/components/post_list/post_list.component';
import {MessageSendSuccessComponent} from './app/components/message_send_success/message_send_success.component';
import {UploadMediaComponent} from './app/components/upload_media/upload_media.component';
import {FindUserFormContextComponent} from './app/components/find_user_form_context/find_user_form_context.component';
import {CreatePostFormComponent} from './app/components/create_post_form/create_post_form.component';
import {ResetPasswordComponent} from './app/components/reset-password/reset-password.component';
import {ForgotPasswordComponent} from './app/components/forgot-password/forgot-password.component';
import {LoginFormComponent} from './app/components/login-form/login-form.component';
import {RegisterFormComponent} from './app/components/register-form/register-form.component';
import {LikeComponent} from './app/components/like/like.component';
import {InstagramComponent} from './app/components/instagram/instagram.component';
import {SearchInputComponent} from './app/components/search_input/search_input.component';

angular.module('app.components')
	.component('peopleSearch', PeopleSearchComponent)
	.component('userProfile', UserProfileComponent)
	.component('acceptBattleRequest', AcceptBattleRequestComponent)
	.component('notification', NotificationComponent)
	.component('postList', PostListComponent)
	.component('messageSendSuccess', MessageSendSuccessComponent)
	.component('uploadMedia', UploadMediaComponent)
	.component('findUserFormContext', FindUserFormContextComponent)
	.component('createPostForm', CreatePostFormComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('loginForm', LoginFormComponent)
	.component('registerForm', RegisterFormComponent)
	.component('like', LikeComponent)
	.component('instagram', InstagramComponent)
	.component('searchInput', SearchInputComponent);
