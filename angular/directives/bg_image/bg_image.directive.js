class BgImageController{
    constructor(){
        'ngInject';
    }
}

export function BgImageDirective(){
    return {
        controller: BgImageController,
        link: function(scope, element, attrs, controllers){

            attrs.$observe('bgImage', function(value) {

                if(!value) value = attrs.placeholderImage;

                element.css({
                    'background-image': 'url(' + value +')'
                });
            });
        }
    }
}
