class ImageResolveController{
    constructor(){
        'ngInject';

        //
    }

    profilePic(user){

        if(user.profile_pic && user.profile_pic.preview_url)
            return user.profile_pic.preview_url;

        if(user.profile_pic && user.profile_pic.file_name)
            return user.profile_pic.file_name;

        return '/img/profile.png'; 
    }
}

export function ImageResolveDirective(){
    return {
        scope:{
            imageSrc : '=imageSrc',
            imageResolve : '=',
        },
        controller: ImageResolveController,
        link: function(scope, element, attrs, ctrl){

            let imageType = attrs.imageType;

            let imagePlacement = attrs.placement;

            attrs.$observe('imageResolve', function(value) {

                if(!value) value = attrs.placeholderImage;

                element.attr('src', ctrl[imageType](scope.imageResolve));

                if(imagePlacement === 'background')
                {
                    element.css({
                        'background-image': 'url(' + value +')'
                    });
                }

            });
        }
    }
}
