class InputFocusController{
    constructor(){
        'ngInject';

        //
    }
}

export function InputFocusDirective(){
    return {
        controller: InputFocusController,
        link: function(scope, element, attrs, controllers){

            $(element).click(()=>{
                $(attrs.inputFocus).focus();
            });
        }
    }
}
