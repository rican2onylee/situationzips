

export default class RlModal{
    constructor( element, scope, parent ){

        this.setElement( element, scope, parent );
    }

    setElement( element, scope, parent ){

        var children = element.children();

        this.$element = angular.element(children[0]);

        this.$element.attr( 'id', element.attr('id') );

        this.$scope = scope;

        element.attr('id', element.attr('id') + '_container' );

        this.$parent  = parent ? parent : angular.element(document.body); 
    }

    open( params ){

        this.$scope.data = params;
        this.$scope.vm   = this;
        this.$scope.vm.isOpen = true;

        this.$element.addClass('rl-modal--open');

        let margin = -(this.$element[0].offsetWidth/2);
        this.$element.css('marginLeft', `${margin}px`); 

        setTimeout(()=>{
            this.$element.addClass('rl-modal--in');   
            this.$parent.addClass('rl-modal-bg--open');        
        }, 200);
    }

    close(){
        this.$element.removeClass('rl-modal--in'); 

        setTimeout(()=>{
            this.$element.removeClass('rl-modal--open'); 
            this.$parent.removeClass('rl-modal-bg--open');
            this.$scope.data = {};      
        }, 100);
    }
}