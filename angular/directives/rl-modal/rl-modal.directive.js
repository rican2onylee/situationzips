import template from './rl-modal.directive.html';
import modal from './rl-modal.class.js';

export function RlModalDirective(RlModalService) {
    'ngInject';    
    return {
        template: template,
        restrict: 'E',
        transclude: true,
        link: function(scope, element, attrs, ctrl){

            var m = new modal( element, scope );

            RlModalService.addModal( attrs.id, m );
        }
    }
}
